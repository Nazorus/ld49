using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegEnemy : MonoBehaviour
{
    public float speed = 10f;
    public float stompDelay = 1f;

    private float directionY = 1f;

    private Rigidbody2D rgbd;
    private Obstacle obstacle;

    public void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        obstacle = GetComponent<Obstacle>();

        StartCoroutine(GoUpAndDown());
    }

    public IEnumerator GoUpAndDown()
    {
        while (!obstacle.dead)
        {
            rgbd.AddForce(Vector2.up * speed * directionY, ForceMode2D.Force);
            directionY *= -1f;
            yield return new WaitForSeconds(stompDelay);
        }
    }
}
