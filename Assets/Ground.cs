using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        IcoController ico = collision.gameObject.GetComponent<IcoController>();
        if (ico != null) ico.SetGrounded();
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        IcoController ico = collision.gameObject.GetComponent<IcoController>();
        if (ico != null) ico.SetGrounded();
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        IcoController ico = collision.gameObject.GetComponent<IcoController>();
        if (ico != null) ico.SetUngrounded();
    }
}
