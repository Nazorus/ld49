using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public IcoController ico;

    public float fillSpeed = 10f;

    public float colorSpeed = 10f;

    private float previousHealth = 100f;

    public Image bar;
    public Image bottom;
    public Image top;
    public Text text;

    public void Start()
    {
        bar = GetComponent<Image>();
    }

    public void Update()
    {
        // fill amount
        bar.fillAmount = Mathf.Lerp(bar.fillAmount, ico.health / ico.maxHealth, Time.deltaTime * fillSpeed);
    
        // flash red if damaged
        if (previousHealth > ico.health)
        {
            bar.color = Color.red;
            bottom.color = Color.red;
            top.color = Color.red;
            //text.color = Color.red;
        }

        // restore color
        bar.color = Color.Lerp(bar.color, Color.white, Time.deltaTime * colorSpeed);
        bottom.color = Color.Lerp(bottom.color, Color.white, Time.deltaTime * colorSpeed);
        top.color = Color.Lerp(top.color, Color.white, Time.deltaTime * colorSpeed);
        //text.color = Color.Lerp(text.color, Color.white, Time.deltaTime * colorSpeed);

        // register health
        previousHealth = ico.health;
    }
}
