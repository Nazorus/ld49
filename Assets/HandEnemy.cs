using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandEnemy : MonoBehaviour
{
    // movement
    [Header("Follow")]
    public float speed = 10f;
    public float followDistance = 80f;

    public float height = 28f;
    public float heightSpeed = 4f;

    // attack
    public float attackDistance = 1f;
    public float attackSpeed = 24f;

    public float attackDuration = 2f;
    public float attackCooldown = 2f;
    private bool attacking = false;
    private bool canAttack = true;

    // player
    private GameObject ico;

    // components
    private Rigidbody2D rgbd;
    private Obstacle obstacle;
    private SpriteRenderer spriteRenderer;

    public void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        obstacle = GetComponent<Obstacle>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        ico = GameObject.FindWithTag("Player");
    }

    public void FixedUpdate()
    {
        if (obstacle.dead) return;

        // caculate direction to ico
        Vector2 toIco = ico.transform.position - transform.position;
        toIco.y = 0;

        // flip sprite
        spriteRenderer.flipX = toIco.x > 0;

        // attack!
        if (attacking)
        {
            // move down and nothing else
            rgbd.AddForce(Vector2.down * attackSpeed);
            return;
        }

        // check attack distance
        if (Mathf.Abs(toIco.x) <= attackDistance && canAttack)
        {
            StartCoroutine(Attack());
        }
        else
        {
            // maintain height
            Vector2 position = rgbd.position;
            position.y = Mathf.Lerp(position.y, height, Time.deltaTime * heightSpeed);
            rgbd.MovePosition(position);

            // follow ico
            if (Mathf.Abs(toIco.x) <= followDistance)
            {
                rgbd.AddForce(toIco * speed);
            }
        }
    }

    private IEnumerator Attack()
    {
        // set flags
        attacking = true;
        canAttack = false;

        // stop attacking after duration
        yield return new WaitForSeconds(attackDuration);
        attacking = false;

        // unset flag after cooldown
        yield return new WaitForSeconds(attackCooldown);
        canAttack = true;
    }
}
