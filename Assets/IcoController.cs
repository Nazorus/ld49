using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IcoController : MonoBehaviour
{
    [Header("Health")]
    public float maxHealth = 100f;
    public float health = 100f;

    private bool invulnerable = false;

    [Header("Movement")]
    public float speed = 16000f;
    public float zoomiesSpeed = 24000f;

    public float maxAirTime = 0.25f;
    private float airTime;

    public float jumpImpulse = 100f;

    public float bumpImpulse = 10f;

    private bool grounded;

    public float dashSpeed = 64f;
    
    public float dashCooldown = 2f;
    private bool canDash = true;

    public float groundSpeedModifier = 1.2f;

    public float jumpCooldown = 0.1f;
    public bool canJump = true;

    // attack
    [Header("Attack")]
    public AttackZone attackZone;
    public Vector2 attackZoneOffset;
    
    public float attackDuration = 0.25f;
    private float attackTimer = 0f;

    public float slashCooldown = 0.5f;
    private bool canSlash = true;

    // zoomies
    [Header("Zoomies")]
    public float maxZoomies = 100f;
    public float zoomiesLevel = 0f;

    public float zoomiesDecay = 10f;

    public bool isZoomies = false;

    public float slowdown = 0.1f;
    public float slowdownDuration = 0.2f;

    // sprites
    [Header("Sprites")]
    public SpriteRenderer normalSprite;
    public SpriteRenderer zoomiesSprite;
    public Animator animator;
    public Animator zoomiesAnimator;

    // animations
    [Header("Animations")]
    public GameObject jumpParticles;
    public Vector2 jumpParticlesOffset;

    public GameObject runParticles;
    public Vector2 runParticlesOffset;
    public float dashThreshold = 2f;

    public GameObject dashParticles;

    public float flipSpeed = 12f;

    public float dashScaleSpeed = 32f;
    public float dashScaleY = 0.5f;
    public float dashScaleX = 1.5f;

    public float jumpScaleSpeed = 32f;
    public float jumpScaleY = 1.5f;
    public float jumpScaleX = 0.5f;

    [Header("External")]
    public ZoomiesPostProcess zoomiesPostProcessing;
    public FollowIco icoCamera;
    public MusicPlayer musicPlayer;

    private SectionManager sectionManager;

    // forward
    private bool isForward = true;

    // components
    private Rigidbody2D rgbd;
    private IcoAudio icoAudio;

    public void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        icoAudio = GetComponent<IcoAudio>();
        sectionManager = GameObject.FindWithTag("Respawn").GetComponent<SectionManager>();
    }

    public void Update()
    {
        // attack
        if (Input.GetButtonDown("Fire1"))
        {
            Attack();
        }

        // attack timer
        if (attackTimer <= 0f)
        {
            attackZone.gameObject.SetActive(false);
        }
        else
        {
            attackTimer -= Time.deltaTime;
        }

        // attack animation
        zoomiesAnimator.SetFloat("AttackTimer", attackTimer);

        // quit game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void LateUpdate()
    {
        // flip animation
        float angle = isForward ? 0f : 180f;
        normalSprite.transform.localRotation = Quaternion.Lerp(normalSprite.transform.localRotation, Quaternion.Euler(0f, angle, 0f), flipSpeed * Time.deltaTime);
        zoomiesSprite.transform.localRotation = Quaternion.Lerp(normalSprite.transform.localRotation, Quaternion.Euler(0f, angle, 0f), flipSpeed * Time.deltaTime);
    }

    public void FixedUpdate()
    {
        // horizontal movement
        float horizontalAxis = Input.GetAxis("Horizontal");
        float moveSpeed = isZoomies ? zoomiesSpeed : speed;

        // ground speed modifier
        if (grounded)
        {
            moveSpeed *= groundSpeedModifier;
        }

        if (horizontalAxis != 0f)
        {
            // round up
            horizontalAxis = Mathf.Sign(horizontalAxis);

            // add force to body
            rgbd.AddForce(Vector2.right * horizontalAxis * moveSpeed * Time.deltaTime);

            // flip sprite according to direction
            //spriteRenderer.flipX = horizontalAxis < 0f;
            isForward = horizontalAxis >= 0f;

            // show dash particles if grounded and changing speed
            if (grounded && (rgbd.velocity.magnitude <= dashThreshold || Mathf.Sign(rgbd.velocity.x) != Mathf.Sign(horizontalAxis)))
            {
                ShowDashParticles();
            }
        }

        // jump
        if (Input.GetButton("Jump") && grounded && canJump)
        {
            // impulse
            rgbd.AddForce(Vector2.up * jumpImpulse, ForceMode2D.Impulse);

            // show particles
            Vector3 offset = jumpParticlesOffset;
            Vector3 direction = rgbd.velocity.normalized;
            Destroy(Instantiate(jumpParticles, transform.position + offset, Quaternion.LookRotation(Vector3.up + direction)), 2f);

            // unset flag
            SetUngrounded();

            // trigger cooldown
            StartCoroutine(TriggerJumpCooldown());

            // play audio
            icoAudio.PlayJump();
        }
        else if (Input.GetButton("Jump") && airTime > 0)
        {
            // reduce jumping force according to air time consumed
            rgbd.AddForce(Vector3.up * jumpImpulse * (airTime / maxAirTime), ForceMode2D.Impulse);

            // count air time
            airTime -= Time.deltaTime;
        }

        // dash
        if (Input.GetButton("Fire3") && canDash)
        {
            // impulse
            rgbd.AddForce(Vector2.right * GetFlipModifier() * dashSpeed, ForceMode2D.Impulse);

            // trigger cooldown
            StartCoroutine(TriggerDashCooldown());
            StartCoroutine(DistortSpritesHorizontal(dashScaleX, dashScaleY, dashScaleSpeed));

            // play audio
            icoAudio.PlayDash();

            // show particles
            float angle = isForward ? 0 : 180;
            Destroy(Instantiate(dashParticles, transform.position, Quaternion.Euler(0f, angle, 0f)), 2f);
            if (grounded)
            {
                ShowDashParticles();
            }
        }

        // don't allow disjointed jump
        if (Input.GetButtonUp("Jump") && !grounded)
        {
            airTime = 0;
        }

        // zoomies
        if (isZoomies)
        {
            zoomiesLevel -= Time.deltaTime * zoomiesDecay;
            if (zoomiesLevel <= 0f)
            {
                // end zoomies
                zoomiesLevel = 0f;
                isZoomies = false;

                zoomiesPostProcessing.End();
                zoomiesSprite.enabled = false;
                normalSprite.enabled = true;

                // switch track
                musicPlayer.SwitchTracks(false);
                
                // FIXME???
                Time.timeScale = 1f;
            }
        }

        // animation
        animator.SetFloat("Speed", Mathf.Abs(horizontalAxis * moveSpeed));
        animator.SetBool("Jumping", !grounded);
        zoomiesAnimator.SetFloat("Speed", Mathf.Abs(horizontalAxis * moveSpeed));
        zoomiesAnimator.SetBool("Jumping", !grounded);

        // check out of bounds
        if (transform.position.y <= -400f)
        {
            Hurt(transform.position + Vector3.up, 999f);
        }
    }

    private void ShowDashParticles()
    {
        // calculate offset and rotation
        Vector3 offset = runParticlesOffset;
        offset.x *= GetFlipModifier();
        Quaternion rotation = Quaternion.Euler(0f, -90f * GetFlipModifier(), 0f);

        // show particles
        Destroy(Instantiate(runParticles, transform.position + offset, rotation), 1f);
    }

    public float GetFlipModifier()
    {
        //return spriteRenderer.flipX ? -1f : 1f;
        return isForward ? 1f : -1f; ;
    }

    public float GetVelocityX()
    {
        return rgbd.velocity.x;
    }

    private void Attack()
    {
        if (!isZoomies || !canSlash) return;

        // activate attack zone
        attackZone.gameObject.SetActive(true);
        attackZone.PlayAnimation(isForward);

        // move according to flip
        Vector2 offset = attackZoneOffset;
        offset.x *= GetFlipModifier();
        attackZone.transform.localPosition =  offset;

        // set timer
        attackTimer = attackDuration;

        // slash cooldown
        StartCoroutine(TriggerSlashCooldown());

        // play audio
        icoAudio.PlayAttack();
    }

    public void SetGrounded()
    {
        grounded = canJump;
        airTime = maxAirTime;
    }

    public void SetUngrounded()
    {
        grounded = false;
    }

    public void Hurt(Vector3 position, float amount = 20f)
    {
        if (invulnerable) return;

        // take damage
        health -= amount;

        // death?
        if (health <= 0f)
        {
            // play audio
            icoAudio.PlayDeath();

            // reload setion
            sectionManager.LoadSection();

            // reset health and zoomies
            health = maxHealth;
            zoomiesLevel = 0f;
            Time.timeScale = 1f;
            return;
        }

        // bump self
        Vector3 bumpDirection = (transform.position - position).normalized;
        rgbd.AddForce(bumpDirection * bumpImpulse, ForceMode2D.Impulse);

        // increase zoomies
        IncreaseZoomies(amount);

        // play animation
        StartCoroutine(PlayHurtAnimation());

        // camera shake
        icoCamera.Shake();

        // play audio
        icoAudio.PlayDamage();
    }

    public void Heal(float amount)
    {
        // increase health
        health += amount;
        if (health >= maxHealth) health = maxHealth;

        // play audio
        icoAudio.PlayCronch();
    }

    public void IncreaseZoomies(float amount)
    {
        // increase value
        zoomiesLevel += amount;
        if (zoomiesLevel > 100f) zoomiesLevel = 100f;

        // trigger zoomies
        if (!isZoomies && zoomiesLevel >= 100f)
        {
            StartCoroutine(PlayZoomiesAnimation());
            isZoomies = true;
            zoomiesSprite.enabled = true;
            normalSprite.enabled = false;

            // switch track
            musicPlayer.SwitchTracks(true);
        }
    }

    private IEnumerator TriggerDashCooldown()
    {
        canDash = false;
        yield return new WaitForSeconds(dashCooldown);
        canDash = true;
    }

    private IEnumerator TriggerSlashCooldown()
    {
        canSlash = false;
        yield return new WaitForSeconds(slashCooldown);
        canSlash = true;
    }

    private IEnumerator TriggerJumpCooldown()
    {
        canJump = false;
        yield return new WaitForSeconds(jumpCooldown);
        canJump = true;
    }

    private IEnumerator DistortSpritesHorizontal(float scaleX, float scaleY, float speed)
    {
        // distort scale
        Vector3 dashScale = new Vector2(scaleX, scaleY);
        while (normalSprite.transform.localScale.x < scaleX - 0.001f || normalSprite.transform.localScale.y > scaleY + 0.001f)
        {
            normalSprite.transform.localScale = Vector3.Lerp(normalSprite.transform.localScale, dashScale, Time.deltaTime * speed);
            zoomiesSprite.transform.localScale = Vector3.Lerp(normalSprite.transform.localScale, dashScale, Time.deltaTime * speed);
            yield return new WaitForEndOfFrame();
        }

        // reset scale
        while (normalSprite.transform.localScale.x > 1f + 0.001f || normalSprite.transform.localScale.y < 1f - 0.001f)
        {
            normalSprite.transform.localScale = Vector3.Lerp(normalSprite.transform.localScale, Vector3.one, Time.deltaTime * speed);
            zoomiesSprite.transform.localScale = Vector3.Lerp(normalSprite.transform.localScale, Vector3.one, Time.deltaTime * speed);
            yield return new WaitForEndOfFrame();
        }
        normalSprite.transform.localScale = Vector3.one;
        zoomiesSprite.transform.localScale = Vector3.one;
    }

    private IEnumerator PlayZoomiesAnimation()
    {
        // enable post process
        zoomiesPostProcessing.Enable();

        // slow down time
        Time.timeScale = slowdown;
        yield return new WaitForSecondsRealtime(slowdownDuration);

        // and gradually restore it
        while (Time.timeScale < 1f)
        {
            yield return new WaitForFixedUpdate();
            Time.timeScale += Time.fixedDeltaTime;
        }

        Time.timeScale = 1f;
    }

    private IEnumerator PlayHurtAnimation()
    {
        if (invulnerable) yield return new WaitForFixedUpdate();

        // flag
        invulnerable = true;

        int blinks = 16;
        while (blinks > 0)
        {
            // alternate between red and white
            Color color = blinks % 2 == 0 ? Color.red : Color.white;
            normalSprite.color = color;
            zoomiesSprite.color = color;

            // count blinks
            blinks--;

            // wait
            yield return new WaitForSeconds(0.066f);
        }

        // restore color
        normalSprite.color = Color.white;
        zoomiesSprite.color = Color.white;

        // flag
        invulnerable = false;
    }
}
