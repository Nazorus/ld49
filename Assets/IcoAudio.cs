using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IcoAudio : MonoBehaviour
{
    public AudioSource mainAudioSource;
    public AudioSource jumpAudioSource;

    public float jumpVolume = 0.5f;
    public AudioClip[] jumpClips;

    public float dashVolume = 0.5f;
    public AudioClip[] dashClips;

    public float attackVolume = 0.66f;
    public AudioClip[] attackClips;

    public float slashVolume = 0.4f;
    public AudioClip[] slashClips;

    public float cronchVolume = 0.33f;
    public AudioClip[] cronchClips;

    public float cronchcronchVolume = 0.6f;
    public AudioClip cronchcronchClip;

    public AudioClip damageClip;
    public float damageVolume = 0.5f;

    public AudioClip deathClip;
    public float deathVolume = 0.75f;

    public void PlayJump()
    {
        AudioClip jumpClip = jumpClips[Random.Range(0, jumpClips.Length)];
        jumpAudioSource.PlayOneShot(jumpClip, jumpVolume);
    }

    public void PlayDash()
    {
        AudioClip clip = dashClips[Random.Range(0, dashClips.Length)];
        mainAudioSource.PlayOneShot(clip, dashVolume);
    }

    public void PlayAttack()
    {
        AudioClip clip = attackClips[Random.Range(0, attackClips.Length)];
        mainAudioSource.PlayOneShot(clip, attackVolume);
    }

    public void PlaySlash()
    {
        AudioClip clip = slashClips[Random.Range(0, slashClips.Length)];
        mainAudioSource.PlayOneShot(clip, slashVolume);
    }

    public void PlayCronch()
    {
        AudioClip clip = cronchClips[Random.Range(0, cronchClips.Length)];
        mainAudioSource.PlayOneShot(clip, cronchVolume);
        mainAudioSource.PlayOneShot(cronchcronchClip, cronchcronchVolume);
    }

    public void PlayDamage()
    {
        mainAudioSource.PlayOneShot(damageClip, damageVolume);
    }

    public void PlayDeath()
    {
        mainAudioSource.PlayOneShot(deathClip, deathVolume);
    }
}
