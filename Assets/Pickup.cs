using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public float wiggleAngle = 30f;
    public float wiggleSpeed = 100f;

    public void Update()
    {
        // wiggle
        transform.localRotation = Quaternion.Euler(0f, 0f, Mathf.Sin(Time.time * wiggleSpeed) * wiggleAngle);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        IcoController ico = collision.gameObject.GetComponent<IcoController>();
        if (ico != null)
        {
            // increase zoomies
            ico.IncreaseZoomies(25f);

            // increase health
            ico.Heal(10f);

            // destroy self
            Destroy(gameObject);
        }
    }
}
