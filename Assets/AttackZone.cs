using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackZone : MonoBehaviour
{
    public IcoController ico;
    public IcoAudio icoAudio;

    public float knockback = 10f;
    public float upwardBias = 2f;

    public float blinkDelay = 0.01f;

    public SpriteRenderer spriteRenderer;

    public void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        // hit obstacle
        Obstacle obstacle = collision.gameObject.GetComponent<Obstacle>();
        if (obstacle != null)
        {
            // calculate direction
            Vector2 direction = (obstacle.transform.position - transform.parent.position) + Vector3.up * upwardBias;
            obstacle.Damage(direction.normalized, knockback);

            // increase zoomies
            ico.IncreaseZoomies(10f);

            // play audio
            icoAudio.PlaySlash();
        }
    }

    public void PlayAnimation(bool foward)
    {
        spriteRenderer.flipX = !foward;
        StartCoroutine(Blink());
    }

    private IEnumerator Blink()
    {
        while (isActiveAndEnabled)
        {
            // transparent
            Color color = spriteRenderer.color;
            color.a = 0.25f;
            spriteRenderer.color = color;
            yield return new WaitForSeconds(blinkDelay);

            // opaque
            color.a = 1f;
            spriteRenderer.color = color;
            yield return new WaitForSeconds(blinkDelay);
        }
    }
}
