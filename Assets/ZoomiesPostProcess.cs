using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ZoomiesPostProcess : MonoBehaviour
{
    private Volume ppVolume;

    private ColorCurves colorCurves;
    private LensDistortion lensDistortion;
    private ChromaticAberration chromaticAberration;
    private Vignette vignette;
    private ColorAdjustments colorAdjustments;

    public float transitionDistortionIntensity = 0.2f;
    public float transitionDistortionScale = 0.01f;
    public float transitionVignette = 1f;
    public float transitionSaturation = -100f;

    public float zoomiesDistortionIntensity = 0.8f;
    public float zoomiesDistortionScale = 1f;
    public float zoomiesVignette = 0f;
    public float zoomiesSaturation = -0f;

    public float defaultDistortionIntensity = 0f;
    public float defaultDistortionScale = 1f;
    public float defaultVignette = 0f;
    public float defaultSaturation = -0f;

    public float transitionSpeed = 50f;
    public float endTransitionSpeed = 20f;
    public float distortionThreshold = 0.001f;

    public void Start()
    {
        ppVolume = GetComponent<Volume>();
        ppVolume.profile.TryGet(out colorCurves);
        ppVolume.profile.TryGet(out lensDistortion);
        ppVolume.profile.TryGet(out chromaticAberration);
        ppVolume.profile.TryGet(out vignette);
        ppVolume.profile.TryGet(out colorAdjustments);
    }

    public void Enable()
    {
        // trigger zoomies effect
        ppVolume.enabled = true;

        StopAllCoroutines();
        // woooooooo
        StartCoroutine(ShowEffects());
    }

    public void End()
    {
        StopAllCoroutines();
        StartCoroutine(StopEffects());
    }

    private IEnumerator ShowEffects()
    {
        // distort everything
        while (lensDistortion.intensity.value < transitionDistortionIntensity - distortionThreshold
            || lensDistortion.scale.value > transitionDistortionScale + distortionThreshold
            || vignette.intensity.value < transitionVignette - distortionThreshold
            || colorAdjustments.saturation.value > transitionSaturation + distortionThreshold)
        {
            lensDistortion.intensity.value = Mathf.Lerp(lensDistortion.intensity.value, transitionDistortionIntensity, Time.deltaTime * transitionSpeed);
            lensDistortion.scale.value = Mathf.Lerp(lensDistortion.scale.value, transitionDistortionScale, Time.deltaTime * transitionSpeed);
            vignette.intensity.value = Mathf.Lerp(vignette.intensity.value, transitionVignette, Time.deltaTime * transitionSpeed);
            colorAdjustments.saturation.value = Mathf.Lerp(colorAdjustments.saturation.value, transitionSaturation, Time.deltaTime * transitionSpeed);

            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }
        lensDistortion.intensity.value = transitionDistortionIntensity;
        lensDistortion.scale.value = transitionDistortionScale;
        vignette.intensity.value = transitionVignette;
        colorAdjustments.saturation.value = transitionSaturation;

        // enable funky colors and abberation
        colorCurves.active = true;
        chromaticAberration.active = true;

        // undistort
        while (lensDistortion.intensity.value > zoomiesDistortionIntensity + distortionThreshold
            || lensDistortion.scale.value < zoomiesDistortionScale - distortionThreshold
            || vignette.intensity.value > zoomiesVignette + distortionThreshold
            || colorAdjustments.saturation.value < zoomiesSaturation - distortionThreshold)
        {
            lensDistortion.intensity.value = Mathf.Lerp(lensDistortion.intensity.value, zoomiesDistortionIntensity, Time.deltaTime * endTransitionSpeed);
            lensDistortion.scale.value = Mathf.Lerp(lensDistortion.scale.value, zoomiesDistortionScale, Time.deltaTime * endTransitionSpeed);
            vignette.intensity.value = Mathf.Lerp(vignette.intensity.value, zoomiesVignette, Time.deltaTime * transitionSpeed);
            colorAdjustments.saturation.value = Mathf.Lerp(colorAdjustments.saturation.value, zoomiesSaturation, Time.deltaTime * transitionSpeed);

            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }
        lensDistortion.intensity.value = zoomiesDistortionIntensity;
        lensDistortion.scale.value = zoomiesDistortionScale;
        vignette.intensity.value = zoomiesVignette;
        colorAdjustments.saturation.value = zoomiesSaturation;
    }

    private IEnumerator StopEffects()
    {
        // undistort
        while (lensDistortion.intensity.value > defaultDistortionIntensity + distortionThreshold
            || lensDistortion.scale.value < defaultDistortionScale - distortionThreshold
            || vignette.intensity.value > defaultVignette + distortionThreshold
            || colorAdjustments.saturation.value < defaultSaturation - distortionThreshold)
        {
            lensDistortion.intensity.value = Mathf.Lerp(lensDistortion.intensity.value, defaultDistortionIntensity, Time.deltaTime * endTransitionSpeed);
            lensDistortion.scale.value = Mathf.Lerp(lensDistortion.scale.value, defaultDistortionScale, Time.deltaTime * endTransitionSpeed);
            vignette.intensity.value = Mathf.Lerp(vignette.intensity.value, defaultVignette, Time.deltaTime * transitionSpeed);
            colorAdjustments.saturation.value = Mathf.Lerp(colorAdjustments.saturation.value, defaultSaturation, Time.deltaTime * transitionSpeed);

            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }
        lensDistortion.intensity.value = defaultDistortionIntensity;
        lensDistortion.scale.value = defaultDistortionScale;
        vignette.intensity.value = defaultVignette;
        colorAdjustments.saturation.value = defaultSaturation;

        // disable funky colors and abberation
        colorCurves.active = false;
        chromaticAberration.active = false;
    }
}
