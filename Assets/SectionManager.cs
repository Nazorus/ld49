using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SectionManager : MonoBehaviour
{
    [Header("Ico")]
    public IcoController ico;
    public Vector2 icoPosition;

    public GameObject[] sections;

    public Narration narration;

    private GameObject currentSection;

    private int currentIndex = 0;

    public void Start()
    {
        LoadSection();
    }

    public void LoadNextSection()
    {
        currentIndex++;

        if (currentIndex < sections.Length)
        {
            LoadSection();
        }
        
        if (currentIndex == sections.Length -1)
        { 
            narration.ShowNarration("i have conquered the universe", 60f);
        }
    }

    public void LoadSection()
    {
        // destroy previous section
        if (currentSection != null)
        {
            Destroy(currentSection);
        }

        // load section
        currentSection = Instantiate(sections[currentIndex]);

        // place ico
        ico.transform.position = icoPosition;
    }
}
