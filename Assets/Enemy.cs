using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed = 10f;
    public float changeDirectionDelay = 2f;

    private float currentDirection = 1f;

    public float wiggleAngle = 30f;
    public float wiggleSpeed = 100f;

    private Rigidbody2D rgbd;
    private Obstacle obstacle;
    private SpriteRenderer spriteRenderer;

    public void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        obstacle = GetComponent<Obstacle>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        StartCoroutine(ChangeDirection());
    }

    public void Update()
    {
        spriteRenderer.flipX = currentDirection >= 0f;
    }

    public void FixedUpdate()
    {
        // don't move when dead
        if (obstacle.dead) return;
        
        // move
        rgbd.velocity = Vector2.right * currentDirection * speed;

        // wiggle
        rgbd.rotation = Mathf.Sin(Time.time * wiggleSpeed) * wiggleAngle;
    }

    private IEnumerator ChangeDirection()
    {
        while (!obstacle.dead) 
        {
            yield return new WaitForSeconds(changeDirectionDelay);
            currentDirection *= -1f;
        }
    }
}
