using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public bool dead = false;

    public float blinkDelay = 0.2f;

    public GameObject deathParticles;

    // components
    private Rigidbody2D rgbd;
    private SpriteRenderer spriteRenderer;

    public void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        // only damage when not dead
        if (dead) return;

        // check if ico
        IcoController ico = collision.gameObject.GetComponent<IcoController>();
        if (ico != null)
        {
            ico.Hurt(transform.position);
        }
    }

    public void Damage(Vector2 direction, float knockback)
    {
        // remove constraints
        rgbd.constraints = RigidbodyConstraints2D.None;

        // apply force according to attack position and knockback
        rgbd.AddForce(direction * knockback, ForceMode2D.Impulse);
        rgbd.AddTorque(knockback * Mathf.Max(direction.x, direction.y) * 2f);

        // die
        if (!dead)
        {
            Die();
        }
    }

    private void Die()
    {
        // set dead
        dead = true;

        // slowdown
        StartCoroutine(DeathSlowDown());

        // blink
        StartCoroutine(DeathBlink());
    }

    private IEnumerator DeathSlowDown()
    {
        // slowdown
        Time.timeScale = 0.2f;
        yield return new WaitForSecondsRealtime(0.2f);
        Time.timeScale = 1f;

        // wait
        yield return new WaitForSeconds(1f);

        // show particles and destroy self
        Destroy(Instantiate(deathParticles, transform.position, Quaternion.identity), 2f);
        Destroy(gameObject);
    }

    private IEnumerator DeathBlink()
    {
        while (true)
        {
            // transparent
            Color color = spriteRenderer.color;
            color.a = 0.25f;
            spriteRenderer.color = color;
            yield return new WaitForSeconds(blinkDelay);

            // opaque
            color.a = 1f;
            spriteRenderer.color = color;
            yield return new WaitForSeconds(blinkDelay);
        }
    }
}
