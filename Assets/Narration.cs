using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Narration : MonoBehaviour
{
    public float characterDelay = 0.33f;
    public float textDelay = 2f;

    public string firstText = "food is life";
    public string secondText = "all else must perish";

    private Text text;

    public void Start()
    {
        text = GetComponent<Text>();
        StartCoroutine(ShowTexts());
    }

    public void ShowNarration(string narration, float duration = 2f)
    {
        StartCoroutine(ShowText(narration, duration));
    }

    private IEnumerator ShowText(string narration, float duration = 2f)
    {
        text.text = "";
        int length = 1;
        while (length <= narration.Length)
        {
            text.text = narration.Substring(0, length);
            length++;
            yield return new WaitForSeconds(characterDelay);
        }

        yield return new WaitForSeconds(duration);
        text.text = "";
    }

    private IEnumerator ShowTexts()
    {
        StartCoroutine(ShowText(firstText));
        yield return new WaitForSeconds(textDelay + 1f);
        StartCoroutine(ShowText(secondText));
    }
}
