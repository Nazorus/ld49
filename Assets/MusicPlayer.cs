using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    [Header("Params")]
    public float maxVolume = 0.5f;

    public float fadeDelay = 1 / 60f;
    public float fadeStep = 1 / 30f;

    [Header("Audio Sources")]
    public AudioSource baseSource;
    public AudioSource zoomiesSource;

    public void Start()
    {
        baseSource.volume = maxVolume;
        zoomiesSource.volume = 0f;

        // play everything
        baseSource.Play();
        zoomiesSource.Play();
    }

    public void SwitchTracks(bool zoomies)
    {
        StopAllCoroutines();
        if (!zoomies)
        {
            StartCoroutine(SwitchToTrack(baseSource, zoomiesSource));
        }
        else
        {
            StartCoroutine(SwitchToTrack(zoomiesSource, baseSource));
        }
    }

    private IEnumerator SwitchToTrack(AudioSource desiredSource, AudioSource otherSource)
    {
        while (desiredSource.volume < maxVolume)
        {
            desiredSource.volume += fadeStep;
            otherSource.volume -= fadeStep;
            yield return new WaitForSecondsRealtime(fadeDelay);
        }

        desiredSource.volume = maxVolume;
        otherSource.volume = 0f;
    }
}
