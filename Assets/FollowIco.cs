using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowIco : MonoBehaviour
{
    public IcoController ico;

    [Header("Follow")]
    public float speedX = 8;
    public float speedY = 2f;

    public float offsetY = 4f;
    public float offsetX = 4f;

    public float maxAngleY = 15f;
    public float angleSpeed = 8f;

    [Header("Screenshake")]
    public float shakeSpeed = 12f;
    public float recoverySpeed = 4f;

    public float shakeAmplitude = 2f;

    private Vector3 shakeOffset = Vector3.zero;

    public void Start()
    {
        // init position
        transform.position = new Vector3(ico.transform.position.x + offsetX * ico.GetFlipModifier(), ico.transform.position.y + offsetY, transform.position.z);

        // init rotation
        transform.localRotation = Quaternion.Euler(0f, maxAngleY * ico.GetFlipModifier(), 0f);
    }

    public void LateUpdate()
    {
        // smooth follow on X and Y axis
        float posX = Mathf.Lerp(transform.position.x, ico.transform.position.x + offsetX * ico.GetFlipModifier(), speedX * Time.deltaTime);
        float posY = Mathf.Lerp(transform.position.y, ico.transform.position.y + offsetY, speedY * Time.deltaTime);
        Vector3 desiredPosition = new Vector3(posX, posY, transform.position.z);

        // smooth rotation
        Quaternion rotation = Quaternion.Lerp(
            transform.localRotation,
            Quaternion.Euler(0f, maxAngleY * ico.GetFlipModifier(), 0f),
            angleSpeed * Time.deltaTime);

        // shake recovery
        shakeOffset = Vector3.Lerp(shakeOffset, Vector3.zero, Time.deltaTime * recoverySpeed);

        // apply position
        transform.position = desiredPosition + shakeOffset;

        // apply rotation
        transform.localRotation = rotation;
    }

    public void Shake()
    {
        shakeOffset = Random.insideUnitCircle.normalized* shakeAmplitude;
    }
}
