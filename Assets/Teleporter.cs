using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    private SectionManager sectionManager;

    public void Start()
    {
        sectionManager = GameObject.FindWithTag("Respawn").GetComponent<SectionManager>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        sectionManager.LoadNextSection();
    }
}
