using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoomiesBar : MonoBehaviour
{
    public IcoController ico;

    public float fillSpeed = 10f;

    public Image bar;
    public Image bottom;
    public Image top;
    public Text text;

    public Color defaultColor;

    public float colorSpeed = 10f;

    public void Start()
    {
        bar = GetComponent<Image>();
    }

    public void Update()
    {
        // fill amount
        bar.fillAmount = Mathf.Lerp(bar.fillAmount, ico.zoomiesLevel / ico.maxZoomies, Time.deltaTime * fillSpeed);
    
        // color
        if (ico.isZoomies)
        {
            Color currentColor = Color.HSVToRGB((Time.time * colorSpeed) % 1f, 1f, 1f);
            ChangeColors(currentColor);
        }
        else
        {
            ChangeColors(defaultColor);
        }
    }

    private void ChangeColors(Color color)
    {
        bar.color = color;
        bottom.color = color;
        top.color = color;
        //text.color = color;
    }
}
